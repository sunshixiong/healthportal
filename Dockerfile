FROM mcr.microsoft.com/java/jdk:8u192-zulu-alpine
MAINTAINER "pdk"
WORKDIR /var/healthportal
VOLUME /var/healthportal
ADD "common/build/libs/common-0.0.1-SNAPSHOT.war" "/var/healthportal/common.war"
EXPOSE 8082
ENV spring.profiles.active "common,his-test"
ENTRYPOINT ["java","-Dspring.profiles.active=common,his-test","-jar","/var/healthportal/common.war"]