package com.boco.web;

import com.boco.common.entity.Account;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseController {


    //    @Autowired
//    protected CacheManager cacheManager;
    @Autowired
    protected HttpServletRequest request;


    public static final String LOGIN = "login";
    public static final String SMS = "sms";
    public static final String IMAGE = "image";
    public static final String USERINFO = "userinfo";

    /**
     * 获取账户基本信息
     *
     * @return
     */
    protected Account queryUserInfo() {
        return (Account) getCache(USERINFO);
    }

    /**
     * 获取用户id
     *
     * @return
     */
    protected String getUserId() {
        return queryUserInfo().getId();
    }


    protected Object getCache(String key) {
        return SecurityUtils.getSubject().getSession().getAttribute(key);
//        return cacheManager.getCache(LOGIN).get(key);
    }

    protected void setCache(String key, Object object) {
//        cacheManager.getCache(LOGIN).put(key, object);
        SecurityUtils.getSubject().getSession().setAttribute(key, object);
    }

}
