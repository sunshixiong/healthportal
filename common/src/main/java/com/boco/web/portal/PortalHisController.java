package com.boco.web.portal;

import com.boco.common.entity.PortalDepart;
import com.boco.common.entity.PortalHospital;
import com.boco.domain.PortalDepartRepository;
import com.boco.domain.PortalHospitalRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.transaction.Transactional;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/19
 */
//@RestController
//@Transactional
//@Api(tags = "门户医院医生相关操作")
public class PortalHisController {

    @Resource
    PortalDepartRepository portalDepartRepository;

    @Resource
    PortalHospitalRepository portalHospitalRepository;
//
//    @RequestMapping(value = "/addDepart")
//    @ApiOperation("添加特色科室")
//    public PortalHospital addDepart(@RequestBody PortalDepart portalDepart, String portalHospitalId) {
//        PortalHospital portalHospital = portalHospitalRepository.getOne(portalHospitalId);
//        portalDepartRepository.save(portalDepart);
//        portalHospital.getDeparts().add(portalDepart);
//        return portalHospital;
//    }
//
//    @RequestMapping(value = "/deleteDepart")
//    @ApiOperation("删除特色科室")
//    public PortalHospital addDepart(String portalDepartId, String portalHospitalId) {
//        PortalHospital portalHospital = portalHospitalRepository.getOne(portalHospitalId);
//        PortalDepart portalDepart = portalDepartRepository.getOne(portalDepartId);
//        portalHospital.getDeparts().remove(portalDepart);
//        return portalHospital;
//    }

}
