package com.boco.web;

import com.boco.service.StatisticsService;
import com.boco.statistics.pojo.HomePage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/12/3
 */
@Api(tags = "统计相关")
@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    @Resource
    StatisticsService statisticsService;

    @ApiOperation("首页统计")
    @GetMapping(value = "/homepage")
    @RequiresAuthentication
    public HomePage statistics() {
        HomePage homePage = new HomePage();
        homePage.setDoctorNum(statisticsService.doctorNum());
        homePage.setHospitalNum(statisticsService.hospitalNum());
        homePage.setNewsNum(statisticsService.newsNum());
        homePage.setNewsYesterdayNam(statisticsService.newsYesterdayNum());
        homePage.setRetreatNum(statisticsService.retreatNum());
        homePage.setRetreatYesterdayNum(statisticsService.retreatYesterdayNum());
        return homePage;
    }
}
