package com.boco.web;

import com.boco.common.entity.Account;
import com.boco.common.entity.Verify;
import com.boco.domain.AccountRepository;
import com.boco.domain.VerifyRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/28
 */
@RestController
@RequestMapping("/approval")
@Api(tags = "实名认证审核")
public class ApprovalController {

    @Resource
    VerifyRepository verifyRepository;

    @Resource
    AccountRepository accountRepository;

    @ApiOperation("通过审核")
    @RequestMapping(value = "/pass",method = RequestMethod.POST)
    public Object pass(String verifyEntityId) {
        Verify verify = verifyRepository.getOne(verifyEntityId);
        verify.setState(2);
        //启用账号
        Account account = accountRepository.getOne(verify.getUserId());
        account.setStatus(1);
        return "success";
    }

    @ApiOperation("打回审核")
    @RequestMapping(value = "/reject",method = RequestMethod.POST)
    public Object reject(String verifyEntityId) {
        Verify verify = verifyRepository.getOne(verifyEntityId);
        verify.setState(3);
        //停用账号
        Account account = accountRepository.getOne(verify.getUserId());
        account.setStatus(0);
        return "success";
    }

}
