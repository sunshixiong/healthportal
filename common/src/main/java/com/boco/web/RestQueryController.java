package com.boco.web;

import com.boco.common.search.JpqlGenerator;
import com.boco.common.util.ResponseUtils;
import com.boco.domain.ExportSupportJpaRepository;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.support.Repositories;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.rest.webmvc.RootResourceInformation;
import org.springframework.hateoas.PagedResources;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/19
 */
@RepositoryRestController
public class RestQueryController {

    @Resource
    JpqlGenerator jpqlGenerator;

    @Resource
    Repositories repositories;

    private ExportSupportJpaRepository repository(RootResourceInformation resourceInformation) {
        return (ExportSupportJpaRepository) repositories.getRepositoryFor(resourceInformation.getDomainType());
    }

    private Object query(RootResourceInformation resourceInformation, @RequestParam Map<String, String> params, @PathVariable String repository, Pageable pageable, boolean allMatch) throws InvocationTargetException, IllegalAccessException {
        ExportSupportJpaRepository repository1 = repository(resourceInformation);
        Page<Object> page = jpqlGenerator.execQuery(repository1,resourceInformation,params,repository,pageable,allMatch);
        PagedResources.PageMetadata pm = new PagedResources.PageMetadata(page.getSize(), page.getNumber(), page.getTotalElements());
        /*空列表也需要展示字段，以便前端统一解析*/
        if (page.getContent().isEmpty()) {
            return ResponseUtils.emptyResources(pm, repository);
        }
        return new PagedResources<>(page.getContent(), pm, new ArrayList<>());
    }

    //    json-server 风格的查询,_like,_gte,_lt,
    @RequestMapping(value = "/{repository}/query", method = RequestMethod.GET)
    @ResponseBody
    public Object query(RootResourceInformation resourceInformation, @RequestParam Map<String, String> params, @PathVariable String repository, Pageable pageable) throws InvocationTargetException, IllegalAccessException {
        return query(resourceInformation, params, repository, pageable, true);
    }


    //    json-server 风格的查询,_like,_gte,_lt,
    @RequestMapping(value = "/{repository}/keywords", method = RequestMethod.GET)
    @ResponseBody
    public Object keywords(RootResourceInformation resourceInformation, @RequestParam Map<String, String> params, @PathVariable String repository, Pageable pageable) throws InvocationTargetException, IllegalAccessException {
        return query(resourceInformation, params, repository, pageable, false);
    }

    //    json-server 风格的查询,_like,_gte,_lt,
    @RequestMapping(value = "/{repository}/{id}/changeStatus", method = RequestMethod.GET)
    @ResponseBody
    @RequiresRoles("admin")
    public Object changeStatus(RootResourceInformation resourceInformation, @PathVariable String id, Integer status) {
        return repository(resourceInformation).changeStatus(id,status);
    }

}
