package com.boco.web;

import com.boco.domain.OperateLogRepository;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/30
 */
@RestController
public class SystemController {


    @Resource
    ConfigurableApplicationContext context;

    //天气查询  寻找sdk 3h,接入1h
    @RequestMapping(value = "/weather")
    public String weather() {
//        sdk.request
        return "";
    }

    @GetMapping(value = "/configs")
    public Map properties() {
        Map<String, Object> ret = new HashMap<>();
        ret.put("systemProperties", context.getEnvironment().getSystemProperties());
        ret.put("environment", context.getEnvironment().getSystemEnvironment());
        Map<String,Object> values = new HashMap<>();
        context.getEnvironment().getPropertySources().forEach(propertySource -> {
            if(propertySource.getSource() instanceof Map){
                values.put(propertySource.getName(),propertySource.getSource());
            }
        });
        ret.put("values",values);
        return ret;
    }


    @PostMapping(value = "/shutdown")
    public String shutdown() {
        context.close();
        return "success";
    }

//    家庭住址省市区选择:前端做

//    图片上传:资讯产品

//    身份证上传:图片上传

//    消息服务


}
