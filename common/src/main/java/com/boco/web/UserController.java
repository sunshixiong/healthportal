package com.boco.web;

import com.boco.common.entity.Account;
import com.boco.common.entity.BlackList;
import com.boco.common.entity.HealthCard;
import com.boco.common.entity.MessageLog;
import com.boco.domain.AccountRepository;
import com.boco.domain.BlackListRepository;
import com.boco.domain.HealthCardRepository;
import com.boco.domain.MessageLogRepository;
import com.boco.service.FileUpDownService;
import com.boco.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.List;


/**
 * 用户相关信息控制层
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Resource
    private UserService userService;


    @Resource(name = "simpleFileUpDownService")
    FileUpDownService fileUpDownService;

    @ApiOperation(value = "通过id获取用户基本信息", notes = "登陆后可获取")
    @GetMapping("/getAccountById")
    public Account getAccountById(String id) {
        if (StringUtils.isEmpty(id)) {
            id = getUserId();
        }
        return accountRepository.getOne(id);
    }

    @PostMapping(value = "/modifyAccountInfo")
    @ApiOperation(value = "修改个人用户基本信息", notes = "登陆后可操作")
    public boolean modifyAccountInfo(@RequestBody Account account) {
        return userService.modifyAccountInfo(account);
    }

    @ApiOperation(value = "修改密码", notes = "登陆后可操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "oldPwd", value = "旧密码", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "newPwd", value = "新密码", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "confirmPwd", value = "确认新密码", paramType = "query", dataType = "String")})
    @PostMapping(value = "/modifyPassword")
    public boolean modifyPassword(String oldPwd, String newPwd, String confirmPwd) {
        if (newPwd == null || !newPwd.equals(confirmPwd)) {
            throw new RuntimeException("密码不能为空");
        }
        return userService.modifyPassword(oldPwd, newPwd, getUserId());
    }

    @ApiOperation(value = "修改头像", notes = "登陆后可操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "avatarUrl", value = "修改头像地址", paramType = "query", dataType = "String")})
    @PostMapping(value = "/modifyAvatar")
    public boolean modifyAvatar(String avatarUrl) {
        if (StringUtils.isEmpty(avatarUrl)) {
            throw new RuntimeException("头像地址不能为空");
        }
        return accountRepository.setFixedAvatarUrlFor(avatarUrl, getUserId()) > 0;
    }

    @ApiOperation(value = "上传头像", notes = "登陆后可操作")
    @PostMapping(value = "/uploadAvatar")
    public String uploadAvatar(@RequestParam("file") MultipartFile file) {
        String avatarUrl = fileUpDownService.uploadForEntity(file).getUrl();
        if (StringUtils.isEmpty(avatarUrl)) {
            return null;
        }
        accountRepository.setFixedAvatarUrlFor(avatarUrl, getUserId());
        return avatarUrl;
    }


    @ApiOperation(value = "添加就诊人信息", notes = "登陆后可操作")
    @PostMapping(value = "/saveHealthCard")
    public HealthCard saveHealthCard(@RequestBody HealthCard healthCard) {
        healthCard.setUserId(getUserId());
        return healthCardRepository.save(healthCard);
    }

    @ApiOperation(value = "设置默认就诊人", notes = "登陆后可操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "就诊卡绑定信息id", paramType = "query", dataType = "Long", example = "1")})
    @PostMapping(value = "/defaultHealthCard")
    public int defaultHealthCard(String id) {
        return healthCardRepository.setFixedStateFor(id, getUserId());
    }

    @ApiOperation(value = "获取就诊人信息列表", notes = "登陆后可操作")
    @GetMapping(value = "/getHealthCardList")
    public List<HealthCard> getHealthCardList() {
        String userId = getUserId();
        return healthCardRepository.findByUserIdOrderByIdDesc(userId);
    }

    @GetMapping(value = "/getMessageLog")
    @ApiOperation(value = "获取个人中心个人消息列表", notes = "登陆后可操作")
    public List<MessageLog> getMessageLog() {
        return messageLogRepository.findAllByUserIdOrderByLogTimeDesc(getUserId());
    }

    @Resource
    HealthCardRepository healthCardRepository;

    @Resource
    MessageLogRepository messageLogRepository;

    @Resource
    AccountRepository accountRepository;

    @Resource
    BlackListRepository blackListRepository;

    @ApiOperation("停用黑名单")
    @PostMapping(value = "/disableBlackList")
    public boolean disableBlackList(String id, @RequestParam(defaultValue = "30") int dates) {
        BlackList blackList = blackListRepository.getOne(id);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH,dates);
        blackList.setExpireDate(calendar.getTime());
        return true;
    }

    @ApiOperation("启用黑名单")
    @PostMapping(value = "/enableBlackList")
    public boolean enableBlackList(String id) {
        BlackList blackList = blackListRepository.getOne(id);
        blackList.setExpireDate(null);
        return true;
    }
}
