package com.boco.web;

import com.boco.common.entity.CarouselImage;
import com.boco.common.entity.PortalDoctor;
import com.boco.common.entity.PortalHospital;
import com.boco.common.entity.data.Ckeditor;
import com.boco.common.pojo.ImportType;
import com.boco.common.pojo.ResourceData;
import com.boco.domain.CarouselImageRepository;
import com.boco.domain.PortalDoctorRepository;
import com.boco.domain.PortalHospitalRepository;
import com.boco.poi.utils.ImportUtils;
import com.boco.service.FileUpDownService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/6
 */
@RestController
@RequestMapping("/manage")
@Transactional
public class ManagementController {

    @Resource
    CarouselImageRepository carouselImageRepository;

    @Resource
    ObjectMapper objectMapper;

    @Resource(name = "simpleFileUpDownService")
    FileUpDownService fileUpDownService;

    @Resource
    PortalDoctorRepository portalDoctorRepository;
    @Resource
    PortalHospitalRepository portalHospitalRepository;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ApiOperation(value = "上传文件", notes = "type:枚举 HOMEPAGE/NEWS")
    public CarouselImage setImage(CarouselImage carouselImage, MultipartFile file) throws IOException {
        ResourceData resourceData = fileUpDownService.uploadForEntity(file);
        carouselImage.setImageUrl(resourceData.getUrl());
        carouselImage.setInfo(objectMapper.writeValueAsString(resourceData));
        carouselImageRepository.save(carouselImage);
        return carouselImage;
    }
    @PostMapping(value = "/editorUpload")
    @ApiOperation(value = "ckEditor上传文件", notes = "type:枚举 HOMEPAGE/NEWS")
    public Ckeditor setCkeditorImage(@RequestParam("upload") MultipartFile file) throws IOException {
        CarouselImage carouselImage = new CarouselImage();
        carouselImage.setType(CarouselImage.Type.CK_EDITOR);
        setImage(carouselImage,file);
        Ckeditor ckeditor = new Ckeditor();
        ckeditor.setUploaded(true);
        ckeditor.setUrl(carouselImage.getImageUrl());
        return ckeditor;
    }

    @RequestMapping(value = "/download", method = RequestMethod.GET)
    @ApiOperation(value = "查看文件")
    public void getImage(String filename, HttpServletResponse response) throws IOException {
        InputStream inputStream = fileUpDownService.download(filename);
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }


    @RequestMapping(value = "/batImport", method = RequestMethod.POST)
    @ApiOperation(value = "批量导入")
    public String batImport(MultipartFile file, ImportType type) throws IOException {
        InputStream inputStream = file.getInputStream();
        switch (type) {
            case HOSPITAL:
                List<PortalHospital> hospitals = ImportUtils.batImport(inputStream,PortalHospital.class);
                portalHospitalRepository.save(hospitals);
                break;
            case DOCTOR:
                List<PortalDoctor> doctors = ImportUtils.batImport(inputStream,PortalDoctor.class);
                portalDoctorRepository.save(doctors);
                break;
            default:
                return "error";
        }
        return "success";
    }
}
