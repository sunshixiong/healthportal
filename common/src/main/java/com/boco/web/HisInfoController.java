package com.boco.web;

import com.boco.common.converter.RequestFacadeProxy;
import com.boco.common.entity.AdvanceRecord;
import com.boco.common.entity.data.*;
import com.boco.domain.AdvanceRecordRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/20
 */
@RestController
@CrossOrigin
@Api(tags = "预约挂号系统--医院相关接口")
public class HisInfoController extends BaseController {

    @Resource(name = "facadeProxy")
    private RequestFacadeProxy facadeProxy;

    @Resource
    private AdvanceRecordRepository advanceRecordRepository;

    //    医院列表
    @RequestMapping(value = "/hospitals", method = RequestMethod.GET)
    @ApiOperation("医院列表")
//    @Secured("user")
    public Page<Hospital> hospitals(@RequestParam(required = false) Map<Object, Object> arguments) {
        return facadeProxy.getHospitals(arguments);
    }

    /*获取医生列表*/
    @RequestMapping(value = "/doctors", method = RequestMethod.GET)
    @ApiOperation("获取医生列表")
    public Page<Doctor> doctors(@RequestParam(required = false) Map<Object, Object> arguments) {
        return facadeProxy.getDoctors(arguments);
    }

    //    排班列表
    @RequestMapping(value = "/schedules", method = RequestMethod.GET)
    @ApiOperation("")
    public Page<Schedule> schedules(@RequestParam(required = false) Map<Object, Object> arguments) {
        return facadeProxy.getSchedules(arguments);
    }

    //    科室列表
    @RequestMapping(value = "/departs", method = RequestMethod.GET)
    @ApiOperation("科室列表")
    public Page<Depart> departs(@RequestParam(required = false) Map<Object, Object> arguments) {
        return facadeProxy.getDeparts(arguments);
    }


    /*查看预约列表*/
    @RequestMapping(value = "/appointments", method = RequestMethod.GET)
    @ApiOperation("查看预约列表")
    public Page<Appointment> appointments(@RequestParam(required = false) Map<Object, Object> arguments) {
        return facadeProxy.getAppointments(arguments);
    }

    /*确认预约(添加预约)*/
    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    @ApiOperation("确认预约(添加预约)")
    @RequiresAuthentication
    public Map confirm(@RequestParam(required = false) Map<Object, Object> arguments,/* @RequestBody String body,*/ AdvanceRecord advanceRecord) {
        Map map =  facadeProxy.confirm(arguments, "");
        advanceRecord.setAppointmentId(map.get("appointmentId") + "");
        advanceRecordRepository.save(advanceRecord);
        return map;
    }

    /*确认预约(添加预约)*/
    @RequestMapping(value = "/cancelAppointment", method = RequestMethod.POST)
    @ApiOperation("取消预约")
    @RequiresAuthentication
    public String cancelAppointment(String id) {
        AdvanceRecord advanceRecord = advanceRecordRepository.getOne(id);
        advanceRecord.setState(3);
        advanceRecordRepository.save(advanceRecord);
        return "success";
    }


    @RequestMapping(value = "/departGroups", method = RequestMethod.GET)
    @ApiOperation("科室组别")
    public List<String> departTree() {
        List<String> list = new ArrayList<>();
        list.add("内科");
        list.add("外科");
        return list;
    }
}
