package com.boco.statistics.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/12/3
 */
@Data
public class HomePage {

    @ApiModelProperty("医院总数")
    Long hospitalNum;

    @ApiModelProperty("医生总数")
    Long doctorNum;

    @ApiModelProperty("推送总数")
    Long newsNum;

    @ApiModelProperty("昨日推送数")
    Long newsYesterdayNam;

    @ApiModelProperty("退号总数")
    Long retreatNum;

    @ApiModelProperty("昨日退号数")
    Long retreatYesterdayNum;
}
