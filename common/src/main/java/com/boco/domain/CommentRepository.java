package com.boco.domain;

import com.boco.common.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface CommentRepository extends ExportSupportJpaRepository<Comment, String> {
    ;
}
