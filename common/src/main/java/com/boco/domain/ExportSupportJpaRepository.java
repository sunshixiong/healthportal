package com.boco.domain;

import com.boco.common.entity.BaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/30
 */
@NoRepositoryBean
public interface ExportSupportJpaRepository<T extends BaseEntity, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

    @Override
    Page<T> findAll(Pageable pageable);

    @Override
    T findOne(Specification<T> spec);

    @Override
    void delete(ID id);

    @Override
    <S extends T> S save(S entity);

    default T changeStatus(ID id, Integer status) {
        T t = findOne(id);
        t.setStatus(status);
        return save(t);
    }
}
