package com.boco.domain;

import com.boco.common.entity.OperateLog;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface OperateLogRepository extends ExportSupportJpaRepository<OperateLog, String> {
}
