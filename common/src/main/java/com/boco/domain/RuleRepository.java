package com.boco.domain;

import com.boco.common.entity.Rule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface RuleRepository extends ExportSupportJpaRepository<Rule, String> {
}
