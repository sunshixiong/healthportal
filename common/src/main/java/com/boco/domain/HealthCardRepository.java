package com.boco.domain;

import com.boco.common.entity.HealthCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/26
 */
@RepositoryRestResource
@CrossOrigin
//就诊人的增删改查,
public interface HealthCardRepository extends ExportSupportJpaRepository<HealthCard, String>, JpaSpecificationExecutor<HealthCard> {

    /**
     * 根据所属用户id进行筛选,按修改时间排序
     *
     * @param userId 所属用户id
     * @return 健康卡列表
     */
    @RestResource(path = "2.1.0",exported = false)
    @Query("select hc from HealthCard hc where :userId = hc.userId or :userId is null order by hc.updateTime desc")
//    @OrderBy("hc.updateTime desc")
    List<HealthCard> findByUserIdOrderByIdDesc(@Param("userId") String userId);

    @Modifying
    @Transactional
    @Query("update HealthCard h set h.state = (1 - h.state) where h.userId = :userId and (h.id = :id OR h.state = 1)")
    @RestResource(exported = false)
    int setFixedStateFor(@Param("id") String id, @Param("userId") String userId);
}
