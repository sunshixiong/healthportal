package com.boco.domain;

import com.boco.common.entity.MessageLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * 注册用户消息通知基本信息
 */
@RepositoryRestResource
public interface MessageLogRepository extends ExportSupportJpaRepository<MessageLog,String> {

    @RestResource(exported = false)
    List<MessageLog> findAllByUserIdOrderByLogTimeDesc(String accountId);

}
