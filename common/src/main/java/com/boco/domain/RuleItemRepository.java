package com.boco.domain;

import com.boco.common.entity.RuleItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface RuleItemRepository extends ExportSupportJpaRepository<RuleItem, String> {
}
