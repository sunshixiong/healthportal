package com.boco.domain;

import com.boco.common.entity.PortalDoctor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface PortalDoctorRepository extends ExportSupportJpaRepository<PortalDoctor, String> {
}
