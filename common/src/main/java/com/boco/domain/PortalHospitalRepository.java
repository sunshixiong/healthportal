package com.boco.domain;

import com.boco.common.entity.PortalHospital;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface PortalHospitalRepository extends ExportSupportJpaRepository<PortalHospital, String> {
}
