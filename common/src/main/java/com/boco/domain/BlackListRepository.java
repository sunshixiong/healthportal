package com.boco.domain;

import com.boco.common.entity.BlackList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

/**
 * auto generated
 */
@RepositoryRestResource
@Repository
public interface BlackListRepository extends ExportSupportJpaRepository<BlackList, String> {
}
