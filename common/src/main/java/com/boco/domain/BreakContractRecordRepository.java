package com.boco.domain;

import com.boco.common.entity.BreakContractRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface BreakContractRecordRepository extends ExportSupportJpaRepository<BreakContractRecord, String> {
}
