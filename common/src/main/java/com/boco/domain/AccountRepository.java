package com.boco.domain;

import com.boco.common.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * auto generated
 */
@RepositoryRestResource
public interface AccountRepository extends ExportSupportJpaRepository<Account, String> {

    @RestResource(exported = false)
    List<Account> findAllByPhone(String phone);

    @RestResource(exported = false)
    @Query("select a.status from Account a where a.id = :id")
    String selectStatus(@Param("id") String id);

    @RestResource(exported = false)
    @Query("select count(a) from BreakContractRecord a where a.userId = :id and a.type = 0")
    String selectDefaultTimes(@Param("id") String id);

    @Modifying
    @Transactional
    @Query("update Account a set a.avatarUrl = :avatarUrl where a.id = :userId")
    @RestResource(exported = false)
    int setFixedAvatarUrlFor(@Param("avatarUrl") String avatarUrl, @Param("userId") String userId);
}
