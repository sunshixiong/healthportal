package com.boco.domain;

import com.boco.common.entity.PortalDepart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface PortalDepartRepository extends ExportSupportJpaRepository<PortalDepart, String> {
}
