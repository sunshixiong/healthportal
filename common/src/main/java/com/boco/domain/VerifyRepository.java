package com.boco.domain;

import com.boco.common.entity.Verify;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface VerifyRepository extends ExportSupportJpaRepository<Verify, String> {
}
