package com.boco.domain;

import com.boco.common.entity.BreakContractUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface BreakContractUserRepository extends ExportSupportJpaRepository<BreakContractUser, String> {
}
