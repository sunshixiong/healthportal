package com.boco.domain;

import com.boco.common.entity.AdvanceRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/26
 */
@RepositoryRestResource
@Repository
public interface AdvanceRecordRepository extends ExportSupportJpaRepository<AdvanceRecord, String> {
}
