package com.boco.domain;

import com.boco.common.entity.News;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * auto generated
 */
@RepositoryRestResource
public interface NewsRepository extends ExportSupportJpaRepository<News, String> {

    @Query("select count(n) from News n where DATE_FORMAT(n.createTime,'%Y-%m-%d') =:date")
    Long newsDateNum(@Param("date") String date);
}
