package com.boco.anno;

import com.boco.common.converter.AnnotationConverter;
import com.boco.common.converter.ObjectConverter;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/14
 */
@Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
@Documented
public @interface HisConverter {
    Class<? extends ObjectConverter> value() default AnnotationConverter.class;

    Class<?> toClazz();
}
