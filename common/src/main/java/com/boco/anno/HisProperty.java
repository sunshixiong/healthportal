package com.boco.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/14
 */
@Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD})
@Documented
public @interface HisProperty {


    /**
     * @return property name from
     */
    String value() default "";
}
