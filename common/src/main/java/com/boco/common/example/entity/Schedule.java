package com.boco.common.example.entity;

import com.boco.anno.HisConverter;
import com.boco.common.entity.CommonEntity;
import lombok.Data;

import java.util.Date;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/21
 */
@Data
@HisConverter(toClazz = com.boco.common.entity.data.Schedule.class)
public class Schedule implements CommonEntity {


    /*id*/
    private String id;

    /**日期*/
    private Date date;

    /**时段*/
    private String time;
}
