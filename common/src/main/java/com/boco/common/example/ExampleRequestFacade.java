package com.boco.common.example;

import com.boco.common.converter.IRequestFacade;
import com.boco.common.entity.Source;
import com.boco.common.entity.data.*;
import com.boco.service.IConvertService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/26
 */
@Component("exampleRequestFacade")
public class ExampleRequestFacade implements IRequestFacade {

    private static final String BASE_URL = "http://192.168.15.216:8889";

    @Resource
    private RestTemplate restTemplate;

    @Resource
    IConvertService convertService;

    private <T> T getRequest(String path, ParameterizedTypeReference<T> ref, Map<Object, Object> arguments) {
        StringBuilder query = new StringBuilder();
        arguments.forEach((k, v) -> {
            query.append(k).append("=").append(v).append("&");
        });
        ResponseEntity<T> ret = restTemplate.exchange(BASE_URL + path + "?" + query.toString(),
                HttpMethod.GET,
                null,
                ref);
        return ret.getBody();
    }

    private Map postRequest(String path,  Map<Object, Object> arguments,String body) {
        StringBuilder query = new StringBuilder();
        arguments.forEach((k, v) -> {
            query.append(k).append("=").append(v).append("&");
        });
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return restTemplate.postForObject(BASE_URL + path + "?" + query.toString(),
                new HttpEntity<>(body,headers),
                Map.class);
    }

    @Override
    public Page<Hospital> getHospitals(Map<Object, Object> arguments) {
        List<com.boco.common.example.entity.Hospital> list = getRequest("/hospitals", new ParameterizedTypeReference<List<com.boco.common.example.entity.Hospital>>() {
        }, arguments);
        HospitalConverter converter = new HospitalConverter();
        converter.setClazz(Hospital.class);
        return new PageImpl<>(convertService.convert(list));
    }

    @Override
    public Page<Doctor> getDoctors(Map<Object, Object> arguments) {
        List<com.boco.common.example.entity.Doctor> list = getRequest("/doctors", new ParameterizedTypeReference<List<com.boco.common.example.entity.Doctor>>() {
        }, arguments);
        DoctorConverter converter = new DoctorConverter();
        converter.setClazz(Doctor.class);
        return new PageImpl<>(convertService.convert(list));
    }

    @Override
    public Page<Schedule> getSchedules(Map<Object, Object> arguments) {
        List<com.boco.common.example.entity.Schedule> list = getRequest("/schedules", new ParameterizedTypeReference<List<com.boco.common.example.entity.Schedule>>() {
        }, arguments);
        return new PageImpl<>(convertService.convert(list));
    }

    @Override
    public Page<Depart> getDeparts(Map<Object, Object> arguments) {
        List<com.boco.common.example.entity.Depart> list = getRequest("/departs", new ParameterizedTypeReference<List<com.boco.common.example.entity.Depart>>() {
        }, arguments);
        return new PageImpl<>(convertService.convert(list));
    }

    @Override
    public Page<Source> getSources(Map<Object, Object> arguments) {
        List<com.boco.common.example.entity.Source> list = getRequest("/sources", new ParameterizedTypeReference<List<com.boco.common.example.entity.Source>>() {
        }, arguments);
        return new PageImpl<>(convertService.convert(list));
    }

    @Override
    public Page<Appointment> getAppointments(Map<Object, Object> arguments) {
        List<com.boco.common.example.entity.Appointment> list = getRequest("/appointments", new ParameterizedTypeReference<List<com.boco.common.example.entity.Appointment>>() {
        }, arguments);
        return new PageImpl<>(convertService.convert(list));
    }

    @Override
    public Map confirm(Map<Object, Object> arguments, String body) {
        return postRequest("/appointments", arguments,body);
    }
}
