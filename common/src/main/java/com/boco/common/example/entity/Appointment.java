package com.boco.common.example.entity;

import com.boco.anno.HisConverter;
import lombok.Data;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/20
 */
@Data
@HisConverter(toClazz = com.boco.common.entity.data.Appointment.class)
public class Appointment  {
    /*id*/
    private String id;

    //预约医生
    private String doctorId;

    //号源编码
    private String sourceNumber;

    //预约人
    private String userId;

    //就诊人
    private String patientId;

    //支付类型
    private String payType;

}
