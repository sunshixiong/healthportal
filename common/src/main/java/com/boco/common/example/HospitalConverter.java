package com.boco.common.example;

import com.boco.common.bocohis.entity.Depart;
import com.boco.common.converter.AnnotationConverter;
import com.boco.common.converter.SimpleConverter;
import com.boco.common.entity.data.Hospital;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/25
 */
public class HospitalConverter extends AnnotationConverter {

    @Override
    public Object convert(Object source) {
        com.boco.common.bocohis.entity.Hospital src = (com.boco.common.bocohis.entity.Hospital) source;
        Hospital hospital = (Hospital) super.convert(source);
        Map<String, String> outpatient = new HashMap<>();
        outpatient.put("startTime", "8:00");
        outpatient.put("endTime", "17:00");
        hospital.setOutpatient(outpatient);
        List<String> top = new ArrayList<>();
        for (Depart depart : src.getTop()) {
            top.add(depart.getName());
        }
        hospital.setTop(top);
        return hospital;
    }
}