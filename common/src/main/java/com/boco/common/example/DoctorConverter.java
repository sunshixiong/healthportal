package com.boco.common.example;

import com.boco.common.converter.SimpleConverter;
import com.boco.common.entity.data.Doctor;
import com.boco.common.example.entity.Schedule;
import org.springframework.util.ReflectionUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/25
 */
public class DoctorConverter extends SimpleConverter {

    @Override
    public Object convert(Object source) {
        com.boco.common.bocohis.entity.Doctor src = (com.boco.common.bocohis.entity.Doctor) source;
        Doctor doctor = (Doctor) super.convert(source);
        Map<String,String> map = (Map) src.getDetailDoctor();
        doctor.setHospitalName(map.get("yiYuanMingCheng"));
        List<Schedule> schedules = new ArrayList<>();
        Schedule s1 = new Schedule();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        s1.setDate(calendar.getTime());
        s1.setTime("上午");
        schedules.add(s1);
        doctor.setSchedules(schedules);
        return doctor;
    }
}
