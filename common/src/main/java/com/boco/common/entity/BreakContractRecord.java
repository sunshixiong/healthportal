package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/6
 * 违约记录
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "portal_break_contract_record", indexes = {@Index(columnList = "id"), @Index(columnList = "userId")})
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "违约记录")
public class BreakContractRecord extends HasUserEntity {

    @ApiModelProperty("违约时间")
    Date date;
    @ApiModelProperty("违约描述")
    String info;
    @ApiModelProperty("记录类型:0普通,1恢复,2停用")
    Integer type;
}
