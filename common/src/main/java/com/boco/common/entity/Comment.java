package com.boco.common.entity;

import com.boco.common.pojo.ImportType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "appoint_comment", indexes = {@Index(columnList = "id")})
@Entity
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "评论")
public class Comment extends BaseEntity {

    public enum Level {
        @ApiModelProperty("一般")
        NORMAL,
        @ApiModelProperty("不好")
        BAD,
        @ApiModelProperty("很好")
        GOOD
    }

    @ApiModelProperty("预约记录id")
    private String advanceRecordId;

    @ApiModelProperty("医院或医生id")
    private String commentedId;

    @ApiModelProperty("评论对象类型：1-医院，2-医生")
    private ImportType type;

    @ApiModelProperty("评论人姓名")
    private String commentatorName;

    @ApiModelProperty("评论人姓名")
    private String userId;

    @ApiModelProperty("评论人头像地址")
    private String avatarUrl;

    @ApiModelProperty("评论内容")
    private String content;

    @ApiModelProperty("评价等级")
    private Level commentLevel;

}
