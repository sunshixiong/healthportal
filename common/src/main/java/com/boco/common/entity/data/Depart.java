package com.boco.common.entity.data;

import com.boco.common.entity.CommonEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/27
 */
@Data
public class Depart implements CommonEntity {
    @ApiModelProperty("科室详情")
    private String detail;

    @ApiModelProperty("科室名称")
    private String name;

    /**
     * id
     */
    private String id;

    /*
     * 科室组别:内科,外科
     * */
    private String departGroup;
}
