package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/6
 * 预约规则
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "portal_rule", indexes = {@Index(columnList = "id")})
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "规则")
public class Rule extends BaseEntity {
    @ApiModelProperty("规则所属医院")
    String hospitalId;
}
