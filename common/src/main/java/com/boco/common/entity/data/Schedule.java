package com.boco.common.entity.data;

import com.boco.common.entity.CommonEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/21
 */
@Data
public class Schedule implements CommonEntity {

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("*日期")
    private Date date;

    @ApiModelProperty("*时段")
    private String time;
}
