package com.boco.common.entity;

import com.boco.jpa.JsonConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "portal_depart", indexes = {@Index(columnList = "id")})
@Entity
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "居民健康门户--门户科室")
public class PortalDepart extends BaseEntity {

    @ApiModelProperty("详情")
    @Convert(converter = JsonConverter.class)
    @Column(name = "detail")
    private Object detail;

    @ApiModelProperty("科室名称")
    private String name;

    /*
     * 科室组别:内科,外科
     * */
    @ApiModelProperty("科室组别:内科,外科")
    private String departGroup;
}
