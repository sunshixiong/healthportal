package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;
import java.awt.event.AdjustmentListener;
import java.util.Date;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/21
 * 就诊人
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "health_card", indexes = {@Index(columnList = "id"), @Index(columnList = "userId")})
@EntityListeners(AdjustmentListener.class)
@ApiModel(description = "健康卡,就诊卡")
public class HealthCard extends HasUserEntity {

    @ApiModelProperty("就诊人类型:1-成人,2-儿童")
    private String type;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("身份证号码")
    private String idCard;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("居民健康卡")
    private String healthCard;

    @ApiModelProperty("是否为默认就诊人，0-否，1-是")
    private Integer state = 0;

    @ApiModelProperty("创建日期")
    private Date createTime;

    @ApiModelProperty("更新日期")
    private Date updateTime;

    @ApiModelProperty(value = "状态:0失效,1生效", example = "1")
    private int status = 1;
}
