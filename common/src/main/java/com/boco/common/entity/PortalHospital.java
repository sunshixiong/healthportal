package com.boco.common.entity;

import com.boco.jpa.JsonConverter;
import com.boco.poi.anno.ExcelInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "portal_hospital", indexes = {@Index(columnList = "id")})
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "居民健康门户--门户医院")
public class PortalHospital extends BaseEntity {
    @ApiModelProperty("名称")
    @ExcelInfo("医院名称")
    private String name;
    @ApiModelProperty("地址")
    @ExcelInfo("医院地址")
    private String location;
    @ApiModelProperty("编码")
    private String code;
    @ExcelInfo("医院电话")
    @ApiModelProperty("电话")
    private String phone;
    @ApiModelProperty("级别(一级,二级,三级)")
    @ExcelInfo("医院级别")
    private String level;
    @ExcelInfo("医院等级")
    @ApiModelProperty("类别(甲等,乙等)")
    private String category;
    @ApiModelProperty("医院logo地址")
    private String avatarUrl;
    @ApiModelProperty("医院简介")
    private String introduction;
    @ApiModelProperty("星级")
    private String stars;
    @ApiModelProperty("车位数")
    private long parkingLot;
    @ApiModelProperty("最大床位数")
    private long max;
    @ApiModelProperty("详情")
    @Convert(converter = JsonConverter.class)
    @Column(name = "detail")
    private Object detail;
    @ApiModelProperty("医院类型")
    @ExcelInfo("医院类型")
    private String type;

}
