package com.boco.common.entity.data;

import com.boco.common.entity.CommonEntity;
import com.boco.common.example.entity.Schedule;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/21
 * <p>
 * 医生
 */
@Data
public class Doctor implements CommonEntity {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("*名称")
    private String name;

    @ApiModelProperty("*医院id")
    private String hospitalId;

    @ApiModelProperty("医院名称")
    private String hospitalName;

    @ApiModelProperty("*职称")
    private String title;

    @ApiModelProperty("*科室")
    private String depart;

    @ApiModelProperty("*擅长")
    private String skillful;

    @ApiModelProperty("*科室id")
    private String departId;

    @ApiModelProperty("* 医生头像地址")
    private String avatarUrl;

    @ApiModelProperty("* 医生简介")
    private String introduction;

    @ApiModelProperty("级别(国家级专家)")
    private String level;

    @ApiModelProperty("专业")
    private String major;

    @ApiModelProperty("详情")
    private String detail;

    @ApiModelProperty("擅长病症")
    private List<String> experts;


    @ApiModelProperty("最近三天排班")
    List<Schedule> schedules;
}
