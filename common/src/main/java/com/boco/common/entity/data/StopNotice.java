package com.boco.common.entity.data;

import com.boco.common.entity.CommonEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 停诊公告医生信息
 */
@Data
public class StopNotice implements CommonEntity {

    @ApiModelProperty("*名称")
    private String name;

    @ApiModelProperty("*职称")
    private String title;

    @ApiModelProperty("*科室")
    private String depart;

    @ApiModelProperty("* 医生头像地址")
    private String avatarUrl;

    @ApiModelProperty("* 停诊时间段")
    private String stopTime;

}
