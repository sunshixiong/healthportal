package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/6
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "portal_black_list", indexes = {@Index(columnList = "id"), @Index(columnList = "userId")})
@Data
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "黑名单")
public class BlackList extends HasUserEntity {

    @ApiModelProperty("黑名单用户")
    String username;

    @ApiModelProperty("过期时间")
    Date expireDate;

    @Override
    public int getStatus() {
        if (expireDate == null) {
            return 1;
        }
        return new Date().after(expireDate) ? 0 : 1;
    }
}
