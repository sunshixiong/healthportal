package com.boco.common.entity.data;

import com.boco.common.entity.CommonEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/21
 * <p>
 * 医院
 */
@Data
public class Hospital implements CommonEntity {

    @ApiModelProperty("*     * 名称     ")
    private String name;
    @ApiModelProperty("*     * 地址     ")
    private String location;
    @ApiModelProperty("*     * id     ")
    private String id;
    @ApiModelProperty("*     * 编码     ")
    private String code;
    @ApiModelProperty("*     * 电话     ")
    private String phone;
    @ApiModelProperty("*     * 级别(一级,二级,三级)     ")
    private String level;
    @ApiModelProperty("*     * 类别(甲等,乙等)     ")
    private String category;
    @ApiModelProperty("*     * 医院logo地址     ")
    private String avatarUrl;
    @ApiModelProperty("*     * 医院简介     ")
    private String introduction;

    @ApiModelProperty("星级")
    private String stars;

    @ApiModelProperty("门诊开始时间startTime,结束时间endTime")
    private Map<String, String> outpatient;

    @ApiModelProperty("科室排名")
    private List<String> top = new ArrayList<>();

    @ApiModelProperty("车位数")
    private long parkingLot;
    @ApiModelProperty("最大床位数")
    private long max;
}
