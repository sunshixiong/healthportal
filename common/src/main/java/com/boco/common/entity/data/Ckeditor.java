package com.boco.common.entity.data;

import lombok.Data;

/**
 * 描述：
 * ckeditor文件上传返回数据格式
 *
 * @author 12859
 * @date 2018/11/27-14:37
 */
@Data
public class Ckeditor {
    private boolean uploaded;
    private String url;
}
