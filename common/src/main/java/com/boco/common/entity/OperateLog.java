package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/12/3
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "common_operate_log", indexes = {@Index(columnList = "id"), @Index(columnList = "userId")})
@Data
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "操作日志")
public class OperateLog extends HasUserEntity {

    @ApiModelProperty("操作名称")
    String name;

    @ApiModelProperty("操作描述")
    String description;
}
