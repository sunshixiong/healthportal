package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "message_log", indexes = {@Index(columnList = "id"), @Index(columnList = "userId")})
@Entity
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "消息")
public class MessageLog extends HasUserEntity {

    @ApiModelProperty("消息内容")
    private String msg;

    @ApiModelProperty("消息生成时间")
    private Date logTime;

    @ApiModelProperty("消息是否已阅读：0-未阅读，1-已阅读")
    private Integer state;

}
