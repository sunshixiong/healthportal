package com.boco.common.entity.data;

import com.boco.common.entity.CommonEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 消息中心
 */
@Data
public class MessageLog implements CommonEntity {

    @ApiModelProperty("* 主键id")
    private String id;

    @ApiModelProperty("* 用户id")
    private String accountId;

    @ApiModelProperty("* 消息内容")
    private String msg;

    @ApiModelProperty("* 消息生成时间")
    private Date logTime;

    @ApiModelProperty("* 消息是否已阅读：0-未阅读，1-已阅读")
    private Integer state;
}
