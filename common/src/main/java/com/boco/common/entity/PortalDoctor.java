package com.boco.common.entity;

import com.boco.jpa.JsonConverter;
import com.boco.poi.anno.ExcelInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.List;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "portal_doctor", indexes = {@Index(columnList = "id")})
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "居民健康门户--门户医生")
public class PortalDoctor extends BaseEntity {

    @ExcelInfo("医生名称")
    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("就职医院")
    @ExcelInfo("就职医院")
    private String hospital;

    @ApiModelProperty("职称")
    @ExcelInfo("职称")
    private String title;

    @ApiModelProperty("所在科室")
    @ExcelInfo("所在科室")
    private String depart;

    @ApiModelProperty("擅长")
    private String skillful;

    @ApiModelProperty("科室id")
    private String departId;

    @ApiModelProperty("医生头像地址")
    private String avatarUrl;

    @ApiModelProperty("医生简介")
    private String introduction;

    @ApiModelProperty("级别(国家级专家)")
    private String level;

    @ApiModelProperty("专业")
    private String major;

    @ApiModelProperty("详情")
    @Convert(converter = JsonConverter.class)
    @Column(name = "detail")
    private Object detail;

    @ApiModelProperty("擅长病症")
    @ElementCollection
    private List<String> experts;
}
