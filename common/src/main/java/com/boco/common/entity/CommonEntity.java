package com.boco.common.entity;

import java.io.Serializable;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/21
 */
public interface CommonEntity extends Serializable {
    long serialVersionUID = 1L;
}
