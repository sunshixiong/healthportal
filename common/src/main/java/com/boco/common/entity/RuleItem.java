package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/6
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "portal_rule_item", indexes = {@Index(columnList = "id")})
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "规则条目")
public class RuleItem extends BaseEntity {

    @ApiModelProperty("条目编号")
    String number;

    @ApiModelProperty("显示级别")
    String level;

    @ApiModelProperty("内容")
    String content;

    @ApiModelProperty("所属规则")
    String ruleId;
}
