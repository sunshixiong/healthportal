package com.boco.common.entity.data;

import com.boco.common.entity.CommonEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 预约记录评论展示bean
 */
@Data
public class RecordComment implements CommonEntity {

    @ApiModelProperty("* 预约记录id")
    private String advanceRecordId;

    @ApiModelProperty("* 医院基本信息")
    private Hospital hospitalInfo;

    @ApiModelProperty("* 医生基本信息")
    private Doctor doctorInfo;

    @ApiModelProperty("* 医院评论内容")
    private String hospitalCommentContent;

    @ApiModelProperty("* 医生评论内容")
    private String doctorCommentContent;

    @ApiModelProperty("* 医院评论等级")
    private Integer hospitalCommentLevel;

    @ApiModelProperty("* 医生评论等级")
    private Integer doctorCommentLevel;
}
