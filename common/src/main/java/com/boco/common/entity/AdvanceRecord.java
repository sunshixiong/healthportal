package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

/**
 * 预约记录
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "appoint_advance_record", indexes = {@Index(columnList = "id"), @Index(columnList = "userId")})
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "预约记录")
public class AdvanceRecord extends HasUserEntity implements CommonEntity {

    @ApiModelProperty("预约医院名称")
    private String hospitalName;

    @ApiModelProperty("医院id")
    private String hospitalId;

    @ApiModelProperty("科室名称")
    private String departmentName;

    @ApiModelProperty("科室id")
    private String departId;

    @ApiModelProperty("预约医生姓名")
    private String doctorName;

    @ApiModelProperty("医生id")
    private String doctorId;

    @ApiModelProperty("就诊人id")
    private String healthCardId;

    @ApiModelProperty("就诊人姓名")
    private String healthCardName;

    @ApiModelProperty("挂号费")
    private String bookingFee = "13";

    @ApiModelProperty("预约时间")
    private Date bookingTime;

    @ApiModelProperty("就诊时间")
    private Date visitTime;

    @ApiModelProperty(value = "就诊时间段，1-上午,2-下午", example = "1")
    private Integer visitArea;

    @ApiModelProperty(value = "状态，0-未支付，1-已问诊未评价，2-已问诊已评价，3-已取消,4-已支付待问诊", example = "0")
    private Integer state = 0;

    @ApiModelProperty("预约id")
    private String appointmentId;

    @ApiModelProperty("支付类型")
    private String patType;

}
