package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.List;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/6
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "portal_break_contract_user", indexes = {@Index(columnList = "id"), @Index(columnList = "userId")})
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "违约用户")
public class BreakContractUser extends HasUserEntity {

    /**
     * 违约记录列表
     */
    @OneToMany(mappedBy = "userId")
//    @JoinTable(name = "portal_user_records")
    @ApiModelProperty("违约记录列表")
    List<BreakContractRecord> records;
}
