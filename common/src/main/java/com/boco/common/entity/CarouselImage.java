package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/5
 * 轮播图
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "portal_carousel_image", indexes = {@Index(columnList = "id")})
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "轮播图")
public class CarouselImage extends BaseEntity {

    public enum Type {
        @ApiModelProperty("首页")
        HOMEPAGE,
        @ApiModelProperty("健康资讯")
        NEWS,
        @ApiModelProperty("医院")
        HOSPITAL,
        @ApiModelProperty("医生")
        DOCTOR,
        @ApiModelProperty("个人")
        PERSON,
        @ApiModelProperty("CK_EDITOR")
        CK_EDITOR
    }


    @ApiModelProperty("图片链接")
    String imageUrl;

    @ApiModelProperty("图片信息")
    String info;

    @ApiModelProperty("轮播图类型")
    Type type;

}
