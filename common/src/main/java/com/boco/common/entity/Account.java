package com.boco.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Table(name = "common_account", indexes = {@Index(columnList = "id")})
@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "账户")
public class Account extends BaseEntity {

    @ApiModelProperty("用户姓名")
    private String accountName;

    @ApiModelProperty("手机号")
    @Column(unique = true)
    private String phone;

    @ApiModelProperty("用户登录密码（加密）")
    private String password;

    @ApiModelProperty("身份证号")
    private String idCard;

    @ApiModelProperty("居民健康卡")
    private String healthCard;

    @ApiModelProperty("头像地址URL")
    private String avatarUrl;

    @ApiModelProperty("邮箱地址")
    private String email;

    @ApiModelProperty("省")
    private String addressProvince;

    @ApiModelProperty("市")
    private String addressCity;

    @ApiModelProperty("区")
    private String addressArea;

    @ApiModelProperty("详细地址")
    private String addressCountry;

    @JsonIgnore
    private String salt;

    @ApiModelProperty("角色列表")
    private String roles;


    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }
}
