package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/6
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "portal_news", indexes = {@Index(columnList = "id")})
@Entity
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "新闻")
public class News extends BaseEntity {

    @ApiModelProperty(allowableValues = "健康资讯,信息公开")
    String firstType; //健康资讯,信息公开

    @ApiModelProperty(allowableValues = "大众保健,慢病护理,老年人保健,孕产妇保健,婴幼儿护理")
    String secondType;//大众保健,慢病护理,老年人保健,孕产妇保健,婴幼儿护理,

    @ApiModelProperty("咨询内容")
    String content;

    @ApiModelProperty("文章标题")
    String title;

}
