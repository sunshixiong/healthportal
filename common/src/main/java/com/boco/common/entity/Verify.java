package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "verify_info", indexes = {@Index(columnList = "id"), @Index(columnList = "userId")})
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "实名验证")
public class Verify extends HasUserEntity {

    @ApiModelProperty("身份证")
    private String idCard;

    @ApiModelProperty("用户真实姓名")
    private String realName;

    @ApiModelProperty("身份证正面照片URL地址")
    private String aboveUrl;

    @ApiModelProperty("身份证反面照片URL地址")
    private String belowUrl;

    @ApiModelProperty(value = "实名认证认证状态，0-已提交资料，1-审核中，2-审核通过，3-审核未通过", example = "0")
    private Integer state = 0;

    @ApiModelProperty("申请审核时间")
    private Date applicateTime;
}
