package com.boco.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/21
 * <p>
 * 号源
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "portal_source", indexes = {@Index(columnList = "id")})
@EntityListeners(AuditingEntityListener.class)
@ApiModel(description = "门户号源")
public class Source extends BaseEntity implements CommonEntity {

    @ApiModelProperty("所属医院")
    private String hospitalId;

    @ApiModelProperty("所属医生")
    private String doctorId;

    @ApiModelProperty("号源日期")
    private Date date;

    @ApiModelProperty("时间段:   上午,下午")
    private String time;


    @ApiModelProperty("号源总数")
    private Integer total;

    @ApiModelProperty("已预约数")
    private Integer appointed;

//    @JoinTable(name = "portal_source_appointments", indexes = {
//            @Index(name = "appointmentIdIndex", columnList = "appointments")
//    })
//    @ElementCollection
//    private List<String> appointments;

    @ApiModelProperty("等级")
    String grade;
    @ApiModelProperty("医院")
    String hospitalName;
    @ApiModelProperty("类型")
    String type;
    @ApiModelProperty("电话")
    String phoneNumber;
    @ApiModelProperty("地址")
    String address;
}
