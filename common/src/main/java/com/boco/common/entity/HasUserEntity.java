package com.boco.common.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/30
 */
@EqualsAndHashCode(callSuper = true)
@Data
@MappedSuperclass
public class HasUserEntity extends BaseEntity {

    @ApiModelProperty("用户id")
    String userId;

    @Transient
    String verifyStatus;

    @Transient
    String defaultTimes;

    @Transient
    Account user;
}
