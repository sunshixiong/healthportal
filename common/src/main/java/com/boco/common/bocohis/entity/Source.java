package com.boco.common.bocohis.entity;

import com.boco.anno.HisConverter;
import lombok.Data;

import java.util.Date;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/21
 */
@Data
@HisConverter(toClazz = com.boco.common.entity.Source.class)
public class Source {

    //所属医院
    private String hospitalId;

    //所属医生
    private String doctorId;

    //号源日期
    private Date date;

    //时间段:   上午,下午
    private String time;
}
