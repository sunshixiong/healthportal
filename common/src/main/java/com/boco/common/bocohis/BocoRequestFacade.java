package com.boco.common.bocohis;


import com.boco.common.converter.IRequestFacade;
import com.boco.common.entity.Source;
import com.boco.common.entity.data.*;
import com.boco.common.example.DoctorConverter;
import com.boco.common.example.HospitalConverter;
import com.boco.config.BocoProperties;
import com.boco.service.IConvertService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/26
 */
@Service("bocoRequestFacade")
public class BocoRequestFacade implements IRequestFacade {

    @Resource
    private BocoProperties bocoProperties;

    @Resource
    private RestTemplate restTemplate;

    @Resource
    IConvertService convertService;

    private <T> T getRequest(String path, ParameterizedTypeReference<T> ref, Map<Object, Object> arguments) {
        StringBuilder query = new StringBuilder();
        arguments.forEach((k, v) -> {
            query.append(k).append("=").append(v).append("&");
        });
        ResponseEntity<T> ret = restTemplate.exchange(bocoProperties.getHis().getUrl() + path + "?" + query.toString(),
                HttpMethod.GET,
                null,
                ref);
        return ret.getBody();
    }

    private Map postRequest(String path, Map<Object, Object> arguments, String body) {
        StringBuilder query = new StringBuilder();
        arguments.forEach((k, v) -> {
            query.append(k).append("=").append(v).append("&");
        });
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return restTemplate.postForObject(bocoProperties.getHis().getUrl() + path + "?" + query.toString(),
                new HttpEntity<>(body, headers),
                Map.class);
    }

    @Override
    public Page<Hospital> getHospitals(Map<Object, Object> arguments) {
//        http://192.168.15.88:8080/portal_web//getYiYuanList.do
        List<com.boco.common.bocohis.entity.Hospital> list = getRequest("/getYiYuanList.do", new ParameterizedTypeReference<List<com.boco.common.bocohis.entity.Hospital>>() {
        }, arguments);
        HospitalConverter converter = new HospitalConverter();
        converter.setClazz(Hospital.class);
        return new PageImpl<>(convertService.convert(list, converter));
    }

    @Override
    public Page<Doctor> getDoctors(Map<Object, Object> arguments) {
//        /getYongHuList.do?yiYuanId=8a4fa1b86145950101614625781e003a&keShiId=40288fe162f0735e0162f5290a680306&name=
        arguments.put("yiYuanId", arguments.getOrDefault("hospitalId", ""));
        arguments.put("keShiId", arguments.getOrDefault("departId", ""));
        arguments.put("yonghuId", arguments.getOrDefault("id", ""));
        List<com.boco.common.bocohis.entity.Doctor> list = getRequest("/getYongHuList.do", new ParameterizedTypeReference<List<com.boco.common.bocohis.entity.Doctor>>() {
        }, arguments);
        DoctorConverter converter = new DoctorConverter();
        converter.setClazz(Doctor.class);
        return new PageImpl<>(convertService.convert(list, converter));
    }


    @Override
    public Page<Schedule> getSchedules(Map<Object, Object> arguments) {
        return new PageImpl<>(Collections.emptyList());
    }

    @Override
    public Page<Depart> getDeparts(Map<Object, Object> arguments) {
// http://192.168.15.88:8080/portal_web/getKeShiByKeShiId.do?yiYuanId=8a4fa1b8614595010161463069580071&keShiId=40288fe162f0735e0162f1110c5d0005
        arguments.put("yiYuanId", arguments.getOrDefault("hospitalId", ""));
        List<com.boco.common.bocohis.entity.Depart> list = getRequest("/getKeShi.do", new ParameterizedTypeReference<List<com.boco.common.bocohis.entity.Depart>>() {
        }, arguments);
        String departGroup = "" + arguments.getOrDefault("departGroup", "");
        List<Depart> result = convertService.convert(
                list.stream().filter(
                        each -> each.getName() != null && each.getName().contains(departGroup)
                ).collect(Collectors.toList()));
        return new PageImpl<>(result);
    }

    @Override
    public Page<Source> getSources(Map<Object, Object> arguments) {
        return new PageImpl<>(Collections.emptyList());
    }

    @Override
    public Page<Appointment> getAppointments(Map<Object, Object> arguments) {
        return new PageImpl<>(Collections.emptyList());
    }

    @Override
    public Map confirm(Map<Object, Object> arguments, String body) {
        Map ret = new HashMap();
        //boco his无预约
        return ret;
    }
}
