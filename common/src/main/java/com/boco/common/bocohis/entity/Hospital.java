package com.boco.common.bocohis.entity;

import com.boco.anno.HisConverter;
import com.boco.common.entity.CommonEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/21
 *
 * 医院
 */
@Data
@HisConverter(toClazz = com.boco.common.entity.data.Hospital.class)
public class Hospital implements CommonEntity {

    /**
     * 名称
     */
    private String name;
    /**
     * 地址
     */
    private String location;
    /**
     * id
     */
    private String id;
    /**
     * 编码
     */
    private String code;
    /**
     * 电话
     */
    private String phone;
    /**
     * 级别
     */
    private String level;
    /**
     * 类别
     */
    private String category;
    /**
     * 医院logo地址
     */
    private String avatarUrl;
    /**
     * 医院简介
     */
    private String introduction;

    /*星级*/
    private String stars;

    /*门诊开始时间startTime,结束时间endTime*/
    private Map<String, String> outpatient;

    /*科室排名*/
    private List<Depart> top = new ArrayList<>();

    /*车位数*/
    private long parkingLot;
    /*最大床位数*/
    private long max;

    /*详情*/
    private Object detail;
}
