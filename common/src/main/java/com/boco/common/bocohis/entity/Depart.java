package com.boco.common.bocohis.entity;

import com.boco.anno.HisConverter;
import com.boco.common.entity.CommonEntity;
import lombok.Data;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/27
 */
@Data
@HisConverter(toClazz = com.boco.common.entity.data.Depart.class)
public class Depart implements CommonEntity {
    /*科室详情*/
    private String detail;

    /*科室名称*/
    private String name;

    /**
     * id
     */
    private String id;

    /*详情*/
    private Object detailDepart;

    /*
     * 科室组别:内科,外科
     * */
    private String departGroup;
}
