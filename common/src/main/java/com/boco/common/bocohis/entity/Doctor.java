package com.boco.common.bocohis.entity;

import com.boco.anno.HisConverter;
import com.boco.common.entity.CommonEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/21
 * <p>
 * 医生
 */
@Data
@HisConverter(toClazz = com.boco.common.entity.data.Doctor.class)
public class Doctor implements CommonEntity {

    /*id*/
    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 医院id
     */
    private String hospitalId;

    @ApiModelProperty("医院名称")
    private String hospitalName;

    /**
     * 职称
     */
    private String title;

    /**
     * 科室
     */
    private String depart;

    /**
     * 擅长
     */
    private String skillful;

    /**
     * 科室id
     */
    private String departId;

    /**
     * 医生头像地址
     */
    private String avatarUrl;

    /**
     * 医生简介
     */
    private String introduction;

    /*级别(国家级专家)*/
    private String level;

    /*专业*/
    private String major;

    /*详情*/
    private String detail;

    /*擅长病症*/
    private List<String> experts;

    /*最近三天排班*/
    List<Schedule> schedules;

    /*详情*/
    private Object detailDoctor;
}
