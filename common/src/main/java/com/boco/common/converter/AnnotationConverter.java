package com.boco.common.converter;

import com.boco.anno.HisProperty;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.util.Properties;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/15
 */
public class AnnotationConverter extends SimpleConverter {
    @Override
    protected Properties getProperties(Object from) {
        Properties properties = new Properties();
        Class<?> clazz = from.getClass();
        ReflectionUtils.doWithFields(clazz, field -> {
            HisProperty hisProperty = field.getAnnotation(HisProperty.class);
            String prop;
            if (hisProperty == null) {
                prop = field.getName();
            } else {
                prop = hisProperty.value();
                if (StringUtils.isEmpty(prop)) {
                    prop = field.getName();
                }
            }
            properties.setProperty(prop, field.getName());
        });
        return properties;
    }
}
