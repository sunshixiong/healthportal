package com.boco.common.converter;

import com.boco.common.util.Utils;

import java.util.Properties;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/15
 */
public class SimpleConverter implements ObjectConverter {

    @Override
    public Object convert(Object source) {
        try {
            Object to = getClazz().newInstance();
            Utils.copyProperties(source, to, getProperties(source));
            return to;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }

    }

    /*需要属性替换的,返回属性映射表*/
    protected Properties getProperties(Object source) {
        return null;
    }

    public Class<?> getClazz() {
        return this.toClazz;
    }

    private Class<?> toClazz = null;

    @Override
    public void setClazz(Class<?> toClazz) {
        this.toClazz = toClazz;
    }
}
