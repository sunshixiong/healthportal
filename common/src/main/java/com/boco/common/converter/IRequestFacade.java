package com.boco.common.converter;

import com.boco.common.entity.Source;
import com.boco.common.entity.data.*;
import org.springframework.data.domain.Page;

import java.util.Map;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/26
 *
 * 统一转接接口
 */
public interface IRequestFacade {

    /*获取医院列表*/
    Page<Hospital>  getHospitals(Map<Object, Object> arguments);

    /*获取医生列表*/
    Page<Doctor> getDoctors(Map<Object, Object> arguments);

    /*获取排班列表*/
    Page<Schedule> getSchedules(Map<Object, Object> arguments);

    /*获取科室列表*/
    Page<Depart> getDeparts(Map<Object, Object> arguments);

    /*获取号源列表*/
    Page<Source> getSources(Map<Object, Object> arguments);

    Page<Appointment> getAppointments(Map<Object, Object> arguments);

    /*确认预约*/
    Map confirm(Map<Object, Object> arguments, String body);
}
