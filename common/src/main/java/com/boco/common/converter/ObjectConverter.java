package com.boco.common.converter;

import org.springframework.core.convert.converter.Converter;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/25
 */
public interface ObjectConverter extends Converter<Object,Object> {

    Class<?> getClazz();

    void setClazz(Class<?> toClazz);
}
