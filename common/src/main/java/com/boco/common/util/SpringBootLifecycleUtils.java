package com.boco.common.util;

import org.springframework.boot.ApplicationPid;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.system.ApplicationPidFileWriter;

import java.io.File;
import java.io.IOException;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/26
 */
public class SpringBootLifecycleUtils {

    private static File getFile() {
        File file = new File("tmp.txt");
        String absp = file.getAbsolutePath();
        return new File(new File(absp).getParentFile().getParentFile(), "bin/application.pid");
    }

    public static void run(Class source, String[] args) {
        try {
            new ApplicationPid().write(getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        SpringApplication application = new SpringApplication(source);
        application.run(args);
    }
}
