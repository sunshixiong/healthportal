package com.boco.common.util;

import com.boco.config.SMSConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.DigestUtils;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * http请求工具
 */
@Slf4j
public class HttpUtils {

    /**
     * 组装拼接短信URL地址参数
     *
     * @return
     */
    public static String createCommonParam() {
        //TODO 根据实际调用的短信接口api的URL连接方式重新修改，以下为示例。
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmSS");
        String timestamp = sdf.format(new Date());
        String sign = DigestUtils.md5DigestAsHex(String.join("", SMSConfig.ACCOUNT_SID, SMSConfig.AUTH_TOKEN, timestamp).getBytes());
        return "&timestamp=" + timestamp + "&sig=" + sign + "&respDataType=" + SMSConfig.RESP_DATA_TYPE;
    }

    /**
     * post请求
     *
     * @param url  功能和操作
     * @param body 需要post的数据
     * @return
     */
    public static String postByRestTemplate(String url, String body) {
        //TODO 实现发送短信验证码功能
        return new RestTemplate().postForEntity(url, body, String.class).getBody();
    }


}
