package com.boco.common.search;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/10/17
 */
@Data
@AllArgsConstructor
public class Part {

    String field;
    Operators flag;
    String value;

    Class<?> type;

    public String outValue() {
        if (Operators.LIKE.equals(flag)) {
            return "%" + value + "%";
        } else {
            return value;
        }
    }
}
