package com.boco.common.search;


/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/30
 */
public enum Operators {

    EQUAL(""),//equal
    LIKE("_like"),//like
    LTE("_lte"),//<=
    GTE("_gte"),//>=
    NE("_ne"),//!=
    ;

    private String flag;

    Operators(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public static Operators fromFlag(String flag) {
        for (Operators value : values()) {
            if (value.getFlag().equals(flag)) {
                return value;
            }
        }
        return null;
    }
}
