package com.boco.common.search;

import com.boco.domain.ExportSupportJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.rest.webmvc.RootResourceInformation;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/9/30
 */
public interface JpqlGenerator {
    Page<Object> execQuery(ExportSupportJpaRepository repository1, RootResourceInformation resourceInformation, Map<String, String> params, String repository, Pageable pageable, boolean allMatch) throws InvocationTargetException, IllegalAccessException;

    Specification geneSpecification(Map<String, String> parameters, Class<?> domainClass,boolean allMatch);
}
