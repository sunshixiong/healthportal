package com.boco.common.pojo;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/20
 */
public enum ImportType {
    @ApiModelProperty("医院")
    HOSPITAL,
    @ApiModelProperty("医生")
    DOCTOR
}
