package com.boco.common.pojo;

import lombok.Data;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/7
 */
@Data
public class ResourceData {

    String url;

    String ext;

    String fileName;

    long length;
}
