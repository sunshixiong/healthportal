package com.boco.config;


/**
 * 短信服务配置类
 */
public class SMSConfig {

    /**
     * 短信key前缀
     */
    public static final String SMS_KEY = "sms_key_";

    /**
     * url前半部分,参数前部分
     */
    public static final String BASE_URL = "https://api.miaodiyun.com/20150822/industrySMS/sendSMS";

    /**
     * 开发者注册后系统自动生成的账号，可在官网登录后查看
     */
    public static final String ACCOUNT_SID = "aac6e373c7534007bf47648ba34ba2f1";

    /**
     * 开发者注册后系统自动生成的TOKEN，可在官网登录后查看
     */
    public static final String AUTH_TOKEN = "47605360a97a4f81bcd576e8e0645edf";

    /**
     * 响应数据类型, JSON或XML
     */
    public static final String RESP_DATA_TYPE = "json";

    /**
     * 加密密码
     */
    public static final String PASSWORD = "";

    /**
     * 注册验证码文本内容
     */
    public static String REGISTER_CONTENT = "【亿阳信通】注册验证码：%s，如非本人操作，请忽略此短信。";

    /**
     * 登录验证码文本内容
     */
    public static String LOGIN_CONTENT = "【亿阳信通】登录验证码：%s，如非本人操作，请忽略此短信。";

    /**
     * 随机生成n为数字字符串
     *
     * @param args 需要生成的位数
     * @return
     */
    public static String code(int... args) {
        int len = (args == null || args.length == 0) ? 6 : args[0];
        len = Math.max(len, 6);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            int c = (int) Math.floor(Math.random() * 10);
            sb.append(c);
        }
        return sb.toString();
    }

    public static String getContent(Integer type, String smsCode) {
        String temp = type == 1 ? SMSConfig.REGISTER_CONTENT : SMSConfig.LOGIN_CONTENT;
        return String.format(temp, smsCode);
    }

}
