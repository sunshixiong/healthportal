package com.boco.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.devtools.autoconfigure.DevToolsProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/7
 */
@Configuration
@EnableAspectJAutoProxy
@EnableJpaAuditing
@EnableSwagger2
@EnableCaching
@Import(SpringDataRestConfiguration.class)
@EnableConfigurationProperties(BocoProperties.class)
public class CommonConfiguration {

    @Bean
    RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        Map<String, Object> map = new HashMap<>();
        map.put("_sort", "desc");
        map.put("limit", 25);
        map.put("_order", "createTime");
        restTemplate.setDefaultUriVariables(map);
        return restTemplate;
    }

}
