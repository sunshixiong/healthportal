package com.boco.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/12/10
 */
@ConfigurationProperties(prefix = "boco")
@Data
public class BocoProperties {

    His his;

    Sms sms;

    @Data
    public static class His {
        String url;
    }

    @Data
    public static class Sms {
        Boolean enable;
        String url;
        String sign;
        String userName;
        String pwd;
    }
}
