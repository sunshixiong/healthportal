package com.boco;

import com.boco.common.util.SpringBootLifecycleUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.boco", "com.boco.config"})
public class CommonApplication {

    public static void main(String[] args) {
        SpringBootLifecycleUtils.run(CommonApplication.class, args);
    }
}
