package com.boco.service;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/29
 */
public interface SmsService {

    String sendSMS(String[] phoneNos,String context) throws Exception;

    String sendSMS(String phone,String content) throws Exception;
}
