package com.boco.service;

import com.boco.common.pojo.ResourceData;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/7
 */
public interface FileUpDownService {

    ResourceData uploadForEntity(MultipartFile file);

    InputStream download(String filename);
}
