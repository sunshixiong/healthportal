package com.boco.service;

import com.boco.anno.HisConverter;
import com.boco.common.converter.ObjectConverter;

import java.util.List;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/14
 */
public interface IConvertService {
    ObjectConverter newInstance(HisConverter hisConverter);

    List convert(List from, ObjectConverter converter);

    List convert(List from);

    Object convert(Object from);
}
