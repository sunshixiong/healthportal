package com.boco.service;

import com.boco.common.entity.Account;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/29
 */
public interface LoginService {

    Account loginInternal(String phone, String password, boolean withoutPwd);
}
