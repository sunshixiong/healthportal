package com.boco.service;

import com.boco.common.entity.Account;

/**
 * 用户信息相关业务接口服务
 */
public interface UserService {

    /**
     * 通过id获取用户信息
     * @param id 用户id
     * @return 用户基本信息
     */
    Account getAccountById(String id);



    /**
     * 修改用户基本信息
     * @param account
     * @return
     */
    boolean modifyAccountInfo(Account account);

    /**
     * 修改密码
     * @param oldPwd 以前的密码
     * @param newPwd 新的密码
     * @param accountId 用户id
     * @return boolean
     */
    boolean modifyPassword(String oldPwd, String newPwd, String accountId);

    /**
     * 修改头像地址
     * @param avatarUrl 头像地址
     * @param accountId 用户id
     * @return boolean
     */
    boolean modifyAvatarUrl(String avatarUrl, String accountId);

    Account registerUser(String phone, String pwd);
}
