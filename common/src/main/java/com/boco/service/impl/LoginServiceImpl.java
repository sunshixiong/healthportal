package com.boco.service.impl;

import com.boco.auth.BocoAuthenticationToken;
import com.boco.common.entity.Account;
import com.boco.domain.AccountRepository;
import com.boco.service.LoginService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/29
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Resource
    AccountRepository accountRepository;

    @Override
    public Account loginInternal(String phone, String password, boolean withoutPwd) {
        if (StringUtils.isEmpty(phone) || StringUtils.isEmpty(password)) {
            throw new RuntimeException("手机号,密码或验证码不能为空！");
        }
        BocoAuthenticationToken usernamePasswordToken = new BocoAuthenticationToken(phone, password, withoutPwd);
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        subject.login(usernamePasswordToken);
        subject.getSession().setTimeout(24* 60 * 60 * 1000);//一天过期
        return accountRepository.findAllByPhone(phone).get(0);
    }
}
