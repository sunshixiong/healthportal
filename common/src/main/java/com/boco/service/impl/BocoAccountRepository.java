package com.boco.service.impl;

import com.boco.auth.Account;
import com.boco.auth.domain.AccountService;
import com.boco.domain.AccountRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/12/4
 */
@Service
public class BocoAccountRepository implements AccountService {

    @Resource
    AccountRepository accountRepository;

    @Override
    public Account findByPhone(String s) {
        List<com.boco.common.entity.Account> list = accountRepository.findAllByPhone(s);
        assert !list.isEmpty();
        com.boco.common.entity.Account account = list.get(0);
        Account ret = new Account();
        ret.setName(account.getAccountName());
        ret.setPassword(account.getPassword());
        ret.setPhone(account.getPhone());
        ret.setRoles(account.getRoles());
        ret.setSalt(account.getSalt());
        return ret;
    }
}
