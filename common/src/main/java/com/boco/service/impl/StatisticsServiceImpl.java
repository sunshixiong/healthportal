package com.boco.service.impl;

import com.boco.domain.NewsRepository;
import com.boco.domain.PortalDoctorRepository;
import com.boco.domain.PortalHospitalRepository;
import com.boco.service.StatisticsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/12/3
 */
@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Resource
    PortalHospitalRepository hospitalRepository;

    @Resource
    PortalDoctorRepository doctorRepository;
    @Resource
    NewsRepository newsRepository;

    @Override
    public Long hospitalNum() {
        return hospitalRepository.count();
    }

    @Override
    public Long doctorNum() {
        return doctorRepository.count();
    }

    @Override
    public Long newsNum() {
        return newsRepository.count();
    }

    @Override
    public Long newsYesterdayNum() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        String date = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        return newsRepository.newsDateNum(date);
    }

    @Override
    public Long retreatNum() {
        return 0L;
    }

    @Override
    public Long retreatYesterdayNum() {
        return 0L;
    }
}
