package com.boco.service;

import com.boco.anno.HisConverter;
import com.boco.common.converter.ObjectConverter;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/11/15
 */
public interface IConvertProvider {

    ObjectConverter newInstance(HisConverter hisConverter);
}
