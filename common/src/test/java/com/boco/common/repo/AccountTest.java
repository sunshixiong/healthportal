package com.boco.common.repo;

import com.boco.common.entity.Account;
import com.boco.domain.AccountRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;

import javax.crypto.Cipher;
import java.util.Date;

public class AccountTest{

    @Autowired
    private AccountRepository accountRepo;

    @Test
    public void saveTest(){
        Account account = new Account();
        account.setAccountName("satan");
        account.setAddressArea("1000000");
        account.setAddressCity("1000001");
        account.setAddressCountry("1000002");
        account.setAddressProvince("100000");
        account.setAvatarUrl("http://img.zcool.cn/community/0117e2571b8b246ac72538120dd8a4.jpg@1280w_1l_2o_100sh.jpg");
        account.setCreateTime(new Date());
        account.setEmail("123@sina.com");
        account.setHealthCard("5522011011120");
        account.setPhone("13500000000");
        account.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        account.setIdCard("511111111111111111111");
        accountRepo.save(account);
        System.out.println("*********************"+ account.getId());
    }


    @Test
    public void testAnnoConverter() throws Exception {
    }


}
