package com.boco.common;

import org.junit.Test;

import java.io.File;
import java.io.PrintWriter;
import java.util.UUID;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2018/12/5
 */

public class DataSqlGeneratorTest {


    @Test
    public void testAddNews() throws Exception {
        File file = new File("tmp.txt");
        PrintWriter fileWriter = new PrintWriter(file);
        for (int i = 0; i < 10000; i++) {
            String id = UUID.randomUUID().toString();
            String sql = "INSERT INTO `healthportal`.`portal_news` (`id`, `createTime`, `status`, `updateTime`, `content`, `html`, `imageUrl`, `type`, `firstType`, `secondType`, `title`) VALUES ('" +
                    id +
                    "', '2018-12-03 14:24:27', '0', '2018-12-03 15:54:14', 'string1111', NULL, NULL, NULL, '健康资讯', '大众保健', 'string1111');";
            fileWriter.println(sql);
        }
        fileWriter.close();
    }
}
