package com.boco.common.converter;

import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

/**
 * @author pandengke/pdkkpdk@163.com
 * @date 2019/3/14
 */
public class JaavTest {
    @Test
    public void testLs() throws Exception {
        String content = "123456";

        String cipherText = encrypt(content);
        System.out.println(cipherText);

        System.out.println(decrypt(cipherText));
    }

    public static String encrypt(String plainText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        byte[] plainBytes = plainText.getBytes();
        SecretKeySpec key = new SecretKeySpec("1234567890123456".getBytes(), "AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] cipherBytes = cipher.doFinal(plainBytes);
        return new String(Base64.getEncoder().encode(cipherBytes));
    }

    public static String decrypt(String cipherText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        SecretKeySpec key = new SecretKeySpec("1234567890123456".getBytes(), "AES");
        cipher.init(Cipher.DECRYPT_MODE, key);// 初始化为解密模式的密码器
        byte[] plainBytes = cipher.doFinal(Base64.getDecoder().decode(cipherText.getBytes()));
        return new String(plainBytes);
    }
}
